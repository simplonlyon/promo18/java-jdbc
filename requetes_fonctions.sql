-- COUNT
SELECT COUNT(*) FROM dog;

-- Nombre de chiens par race
SELECT b.label, COUNT(*) AS CHIENS
    FROM breed b
       JOIN dog d
        ON d.id_breed = b.id
    GROUP BY b.label
    ORDER BY CHIENS;

-- Nombre de chiens par éleveur
SELECT br.name, COUNT(d.id_breed) AS BREEDS
    FROM breeder br
       LEFT JOIN dog d
            ON d.id_breeder = br.id
    GROUP BY br.name
    ORDER BY BREEDS;

-- MIN, MAX, AVG
-- Âge du plus jeune chien
SELECT MIN(age)
    FROM dog;

-- Âge le plus vieux
SELECT MAX(age)
    FROM dog;

-- Âge moyen 
SELECT AVG(age)
    FROM dog;

-- Nom du chien le plus jeune
-- On fait ici appel à une sous-requête
SELECT name, age
    FROM dog
    WHERE age = (
        SELECT MIN(age) FROM dog
    );

-- CONCAT
-- Afficher le nom du chien, et entre parenthèses sa race
SELECT CONCAT(d.name, ' (', b.label, ')') AS CHIEN
    FROM dog d
        INNER JOIN breed b
            ON b.id = d.id_breed;

-- COALESCE
-- Afficher le nom du chien, le nom de son éleveur, et s'il n'a pas d'éleveur, 'Sauvage'
SELECT d.name, COALESCE( br.name, 'Sauvage')
    FROM dog d
        LEFT JOIN breeder br
            ON br.id = d.id_breeder;


-- LIMIT
-- Afficher les 2 premiers chiens
SELECT name
    FROM dog
    LIMIT 2, 3;
