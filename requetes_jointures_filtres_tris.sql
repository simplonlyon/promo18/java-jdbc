SELECT d.name AS NOM, b.name AS RACE
    FROM Dog d, Breed b
    WHERE d.id_breed = b.id;

SELECT d.name AS NOM, b.name AS RACE
    FROM Dog d
    INNER JOIN Breed b
        ON b.id = d.id_breed
    ORDER BY NOM DESC;

SELECT b.name AS RACE, d.name AS NOM
    FROM Dog d
    INNER JOIN Breed b
        ON b.id = d.id_breed
    ORDER BY RACE ASC, NOM DESC;

SELECT b.name AS RACE, d.name AS NOM, d.age
    FROM Dog d
    INNER JOIN Breed b
        ON b.id = d.id_breed
    ORDER BY RACE ASC, age DESC;

SELECT d.name
    FROM Dog d
    INNER JOIN Breed b
        ON b.id = d.id_breed
    WHERE b.name = 'labrador';


SELECT b.name AS ÉLEVEUR, d.name AS CHIEN
    FROM breeder b
    INNER JOIN dog d
        ON d.id_breeder = b.id
    WHERE b.id = 2;

SELECT b.name AS ÉLEVEUR, d.name AS CHIEN
    FROM breeder b
    LEFT JOIN dog d
        ON d.id_breeder = b.id
    WHERE d.id_breeder IS NULL;

SELECT br.name AS ÉLEVEUR, bd.label AS RACE
    FROM breeder br
    LEFT JOIN  dog  d
         ON d.id_breeder = br.id
    LEFT JOIN breed bd
        ON bd.id = d.id_breed;

SELECT DISTINCT br.name AS ÉLEVEUR, bd.label AS RACE,
    FROM breeder br
        INNER JOIN  dog  d
            ON d.id_breeder = br.id
        INNER JOIN breed bd         
            ON bd.id = d.id_breed
    -- WHERE .....
    ORDER BY ÉLEVEUR;
