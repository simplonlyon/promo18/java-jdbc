package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.promo18.entity.Dog;

/**
 * Classe contenant toutes les requêtes SQL nécessaire pour l'entité Dog
 */
public class DogRepository {

    /**
     * Méthode qui renvoie tous les chiens stockés dans la base de données
     * 
     * @return Une liste d'instance de Dog, vide ou non selon ce qu'on a dans la bdd
     */
    public List<Dog> findAll() {
        List<Dog> list = new ArrayList<>();
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                Dog dog = new Dog(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("breed"),
                        rs.getInt("age"));
                list.add(dog);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Méthode pour faire persister un nouveau chien en base de données, qui va
     * consister à convertir une instance de Dog en requête INSERT INTO
     * 
     * @param dog Un chien sans id à faire persister
     */
    public void save(Dog dog) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO dog (name, breed, age) VALUES (?,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            stmt.setInt(3, dog.getAge());

            stmt.executeUpdate();

            /**
             * cette partie là permet de récupérer l'id auto increment par la bdd et de
             * l'assigner à l'instance de Dog passée en argument
             */
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {

                dog.setId(rs.getInt(1));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Méthode qui va récupérer une instance de Dog en se basant sur son id dans la
     * bdd
     * @param id l'id du chien qu'on recherche
     * @return Renvoie soit une instance de Dog, soit null si aucun chien n'a été
     *         trouvé pour cet id
     */
    public Dog findById(int id) {
        // On peut également faire que la méthode renvoie un Optional<Dog> plutôt que
        // soit un chien, soit du null, c'est d'ailleurs ce que fera JPA qu'on utilisera
        // public Optional<Dog> findById(int id) {

        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Dog dog = new Dog(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("breed"),
                        rs.getInt("age"));
                return dog;
                // return Optional.of(dog);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
        // return Optional.empty();
    }
    /**
     * Méthode permettant de supprimer un chien en se basant sur son id
     * @param id l'id du chien à supprimer
     * @return TRUE si la requête a bien supprimé le chien voulu, FALSE sinon
     */
    public boolean deleteById(int id) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM dog WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }
    /**
     * Méthode permettant de mettre à jour un chien existant en base de données
     * Elle va mettre à jour tous les champs du chien donné
     * @param dog Le chien à mettre à jour, il faut que ça soit un chien complet avec id
     * @return TRUE si le chien a bien été modifié, FALSE sinon
     */
    public boolean update(Dog dog) {
        try (Connection connection = DbUtil.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE dog SET name=?,breed=?,age=? WHERE id=?");
            
            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            stmt.setInt(3, dog.getAge());
            stmt.setInt(4, dog.getId());

            return stmt.executeUpdate() == 1;
            

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }
}
