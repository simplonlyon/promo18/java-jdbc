-- Ajout des champs "père" et "mère" dans la table dog
ALTER Table dog
add COLUMN id_father INT UNSIGNED;

ALTER Table dog
add COLUMN id_mother INT UNSIGNED;

ALTER TABLE dog
MODIFY COLUMN id_father INT UNSIGNED;
ALTER TABLE dog
MODIFY COLUMN id_mother INT UNSIGNED;

-- Ajout des clés étrangères
ALTER Table dog
add constraint FK_dog_mother FOREIGN KEY(id_mother) references dog(id);

ALTER Table dog
add constraint FK_dog_father FOREIGN KEY(id_father) references dog(id);

-- Vérification des clés
SELECT 
  TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
FROM
  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  REFERENCED_TABLE_SCHEMA = 'dog_manager' AND
  REFERENCED_TABLE_NAME = 'dog';


-- Jeu d'essai
UPDATE dog
    SET id_father = 1, id_mother = 2
    WHERE id = 3;
UPDATE dog
    SET id_father = 1, id_mother = 2
    WHERE id = 4 OR id = 8;
-- Syntaxe alternative, avec le "IN" :
UPDATE dog
    SET id_father = 1, id_mother = 2
    WHERE id IN (4, 8);

-- Update avec un Between, pour mettre à jour plusieurs enregistrements d'un coup'
UPDATE dog
    SET id_father = 1, id_mother = 2
    WHERE id BETWEEN 5 AND 7;


UPDATE dog
    SET id_father = 5, id_mother = 4
    WHERE id = 9;

UPDATE dog
    SET id_father = 6, id_mother = 7
    WHERE id = 5;



SELECT d.name AS CHIEN, df.name AS PÈRE, dm.name AS MÈRE
    FROM dog AS d
        JOIN dog AS df
            ON df.id = d.id_father
        JOIN dog AS dm
            ON dm.id = d.id_mother
    WHERE d.id = 3;


-- Afficher les chiens qui ont un grand-père (père de leur père ou père de leur mère)
SELECT d.name AS CHIEN, dff.name AS PÈREPÈRE, dmf.name AS PÈREMÈRE
    FROM dog d
        JOIN dog AS df
            ON df.id = d.id_father
        JOIN dog AS dm
            ON dm.id = d.id_mother
        JOIN dog AS dff
            ON dff.id = df.id_father
        JOIN dog AS dmf
            ON dmf.id = dm.id_father;
