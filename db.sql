DROP TABLE IF EXISTS dog;
CREATE TABLE dog (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64),
    breed VARCHAR(64),
    age INT
);

INSERT INTO dog (name, breed, age) VALUES 
("Fido", "Corgi", 3),
("Rex", "Poodle", 13),
("Nougat", "Golden Retriever", 6);